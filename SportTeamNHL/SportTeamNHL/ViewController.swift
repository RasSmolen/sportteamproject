//
//  ViewController.swift
//  SportTeamNHL
//
//  Created by Rastislav Smolen on 06/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import UIKit

class TeamCellController: UITableViewCell
{
    @IBOutlet var teamTextLabel: UILabel!
    
    @IBOutlet var coutTeamMembersLabel: UILabel!
}

class ViewController: UIViewController
{
    
    var textOutput : TextInjection?
    
    var teamConnect : TeamSetUpViewController!
    
    
    let coreDataController = CoreDataContoller.shared

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        teamConnect.addButtonAction{
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//        }
        

 //   }


    }
    
}
extension ViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return coreDataController.fetch(Team.self).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? TeamCellController
        
        cell?.teamTextLabel.text = coreDataController.fetch(Team.self)[indexPath.row].teamName
        
    //    print(cell?.teamTextLabel.text)
        
        //self.tableView.reloadData()

        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    
//    let vc = storyboard?.instantiateViewController(identifier: "TeamSetUpViewController") as? TeamSetUpViewController
//
//        self.navigationController?.pushViewController(vc!, animated: true)
        performSegue(withIdentifier: "sequeToTeamSetUp", sender: nil)

    }
    
    
}


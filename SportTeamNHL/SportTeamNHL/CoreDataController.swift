//
//  CoreDataController.swift
//  SportTeamNHL
//
//  Created by Rastislav Smolen on 06/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import Foundation
import CoreData


class CoreDataContoller
{
    
    
    private init (){}
    
    static let shared = CoreDataContoller()

    var mainContext: NSManagedObjectContext
    {
        return persistentContainer.viewContext
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "SportTeamNHL")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () -> Bool {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                
        
            }
        }
        return false

    }
    func fetch <T :NSManagedObject> (_ objectType : T.Type)-> [T]
    {
        let entityName = String(describing: objectType)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        do
        {
            let fetchedObjects = try mainContext.fetch(fetchRequest) as? [T]
            return  fetchedObjects ?? [T]()
            
        }catch
        {
            print(error)
            return[T]()
        }
    }
}

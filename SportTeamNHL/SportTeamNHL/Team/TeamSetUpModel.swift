//
//  TeamSetUpModel.swift
//  SportTeamNHL
//
//  Created by Rastislav Smolen on 06/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class TeamSetUpModel
{
 var names: [NSManagedObject] = []
    
    let coreDataController = CoreDataContoller.shared
    
    
    func getRandomColor() -> UIColor {
         //Generate between 0 to 1
         let red:CGFloat = CGFloat(drand48())
         let green:CGFloat = CGFloat(drand48())
         let blue:CGFloat = CGFloat(drand48())

         return UIColor(red:red, green: green, blue: blue, alpha: 1.0)
    }
    
    
    
}



//
//  TeamSetUpViewController.swift
//  SportTeamNHL
//
//  Created by Rastislav Smolen on 06/11/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import UIKit
import  CoreData


protocol TextInjection
{
    func textFromTexField(team: String)
}

class TeamSetUpViewController: UIViewController
{
    var model : TeamSetUpModel!
    
    var newTeam = [Team]()
    
    let coreDataController = CoreDataContoller.shared
    
    @IBOutlet var addTeamTextField: UITextField!
    
    @IBOutlet var addTeamButton: UIButton!
    
    @IBOutlet var colorView : UIImageView!
    
    @IBOutlet var pickCollorButton: UIButton!
    
    let didBeginChangeCollor = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        model = TeamSetUpModel()
        
        
        
        
    }

    @IBAction func addButtonAction(_ sender: UIButton )
    {
        let mainContext = coreDataController.mainContext
        
        let newTeam = Team(context: mainContext)

        newTeam.teamName = addTeamTextField.text
        
        addTeamTextField.text = coreDataController.saveContext() ? "Saved" : nil
        
        addTeamTextField.text = nil
        
         getTeam()
     //  completion()

    }
 
    func printTeam()
    {
        newTeam.forEach({print($0.teamName)})

    }
    func getTeam()
    {
        let newTeam = coreDataController.fetch(Team.self)
        self.newTeam = newTeam
        
        printTeam()
    }
    
}

extension TeamSetUpViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

